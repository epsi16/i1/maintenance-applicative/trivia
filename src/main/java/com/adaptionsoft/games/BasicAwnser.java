package com.adaptionsoft.games;

public class BasicAwnser {

    public void printCorrect(){
        System.out.println("Answer was correct!!!!");
    }

    public void printIncorrect(){
        System.out.println("Question was incorrectly answered");
    }

    public void printParagraphe(){
        System.out.println("\n ___________________________________ \n");
    }

    public void printMessage(String message){
        System.out.println(message);
    }

    public void printAskTheme(CategorieEnum choosenTheme){
        System.out.println("1 - " + CategorieEnum.Sport);
        System.out.println("2 - " + CategorieEnum.Science);
        System.out.println("3 - " + CategorieEnum.Pop);
        System.out.println("4 - " + choosenTheme);
    }
}
