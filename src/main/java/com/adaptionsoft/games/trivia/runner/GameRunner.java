
package com.adaptionsoft.games.trivia.runner;

import java.util.Random;
import java.util.Scanner;

import com.adaptionsoft.games.BasicAwnser;
import com.adaptionsoft.games.CategorieEnum;
import com.adaptionsoft.games.model.Player;
import com.adaptionsoft.games.uglytrivia.Game;


public class GameRunner {

    private static boolean notAWinner;

    private static BasicAwnser basicAwnser = new BasicAwnser();

    public static void main(String[] args) {

        try {
            Game aGame = chooseTheme();
            setWinValue(aGame);
            aGame.add(new Player("Cheh"));
            aGame.add(new Player("Michel"));
            aGame.add(new Player("Denis"));
            aGame.add(new Player("Renée"));

            runGame(aGame);
        } catch (Exception e) {
            System.out.println("There are already enough players, max player is 6");
        }
    }

    private static void setWinValue(Game aGame){
        System.out.println("-- Choisir la valeur de la victoire minimum 6 --");
        do{
            Scanner scanner = new Scanner(System.in);
            String text = scanner.nextLine();
            aGame.winValue = Integer.parseInt(text);
            if(aGame.winValue < 6){
                System.out.println("la valeur minimum est 6");
            }
        }while (aGame.winValue < 6);
    }

    private static Game chooseTheme() {
        System.out.println("-- Preferez vous la Techno (1) ou le Rock (2) --");
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();
        Game aGame = new Game();
        if (text.equals("1")) {
            aGame = new Game(CategorieEnum.Techno.toString());
        } else if (text.equals("2")) {
            aGame = new Game(CategorieEnum.Rock.toString());
        }
        return aGame;
    }

    private static void runGame(Game aGame){
        Scanner scanner = new Scanner(System.in);

        Random rand = new Random();

        if (aGame.isPlayable()) {
            do {
                basicAwnser.printParagraphe();
                System.out.println(aGame.players.get(aGame.currentPlayer).getName() + " is the current player");
                boolean hasLeaved = aGame.leaveGame(aGame.players.get(aGame.currentPlayer));

                if (!hasLeaved) {
                    aGame.roll(rand.nextInt(5) + 1);

                    String joker = "non";
                    Player currentPlayer = aGame.getCurentPlayer();
                    if (!currentPlayer.isJokerUsed()) {
                        System.out.println("Voulez vous utiliser un joker ? -Oui? -Non?");
                        joker = scanner.nextLine();
                    }

                    if (joker.equals("oui")) {
                        aGame.useJoker();
                        notAWinner = true;

                    } else {
                        int random = rand.nextInt(3);
                        if (random == 2) {
                            notAWinner = aGame.wrongAnswer();
                        } else {
                            notAWinner = aGame.wasCorrectlyAnswered();
                        }
                    }
                } else {

                   /* aGame.currentPlayer++;*/
                    if (aGame.currentPlayer == aGame.players.size()) aGame.currentPlayer = 0;
                    notAWinner = true;
                }

                if (aGame.getHowManyPlayers() <= 1) {
                    notAWinner = false;
                    System.out.println("Winner " + aGame.players.get(0).getName() + ", everybody has left the game");
                }

            } while (notAWinner);
        } else {
            System.out.println("You need to add player");
        }

    }



}
