package com.adaptionsoft.games.uglytrivia;

import com.adaptionsoft.games.BasicAwnser;
import com.adaptionsoft.games.CategorieEnum;
import com.adaptionsoft.games.model.Player;

import java.util.*;

public class Game {
    BasicAwnser basicAwnser = new BasicAwnser();
    Scanner scanner = new Scanner(System.in);


    public List<Player> players = new ArrayList<>();
    int[] places = new int[7];
    int[] purses = new int[7];
    boolean[] inPenaltyBox = new boolean[7];
    boolean isPreviousPlayerPrison = false;
    CategorieEnum nextPlayerTheme;
    public int winValue = 6;

    LinkedList<String> popQuestions = new LinkedList<>();
    LinkedList<String> scienceQuestions = new LinkedList<>();
    LinkedList<String> sportsQuestions = new LinkedList<>();
    LinkedList<String> choosenQuestions = new LinkedList<>();

    CategorieEnum choosenTheme;

    public int currentPlayer = 0;
    boolean isGettingOutOfPenaltyBox;

    public Game() {
    }

    public Game(String categorieMusic) {
        for (int i = 0; i < 50; i++) {
            popQuestions.addLast(CategorieEnum.Pop + " Question " + i);
            scienceQuestions.addLast((CategorieEnum.Science + " Question " + i));
            sportsQuestions.addLast((CategorieEnum.Science + " Question " + i));
            choosenQuestions.addLast((CategorieEnum.Techno + " Question " + i));

            if (categorieMusic.equals(CategorieEnum.Rock.toString())) {
                choosenTheme = CategorieEnum.Rock;
                choosenQuestions.addLast(CategorieEnum.Rock + " Question " + i);
            } else if (categorieMusic.equals(CategorieEnum.Techno.toString())) {
                choosenTheme = CategorieEnum.Techno;
                choosenQuestions.addLast(CategorieEnum.Techno + " Question " + i);
            }
        }
    }

    public boolean isPlayable() {
        return (getHowManyPlayers() >= 2 && getHowManyPlayers() <= 6);
    }

    public void add(Player player) throws Exception {
        if (getHowManyPlayers() >= 6) {
            throw new Exception("There are already enough players");
        } else {
            players.add(player);
            places[getHowManyPlayers()] = 0;
            purses[getHowManyPlayers()] = 0;
            inPenaltyBox[getHowManyPlayers()] = false;

            System.out.println(player.getName() + " was added");
            System.out.println("They are player number " + getHowManyPlayers());
        }
    }

    public int getHowManyPlayers() {
        return players.size();
    }

    public void roll(int roll) {

        System.out.println("They have rolled a " + roll);


        if (inPenaltyBox[currentPlayer]) {
            Random random = new Random();
            int shouldLeave = random.nextInt(players.get(currentPlayer).getIncPrison()) + 1;
            System.out.println(players.get(currentPlayer).getName() + " have 1/" + players.get(currentPlayer).getIncPrison() + " chance to leave prison");
            if (shouldLeave == 1) {
                isGettingOutOfPenaltyBox = true;
                System.out.println(players.get(currentPlayer).getName() + " is getting out of the penalty box");
                places[currentPlayer] = places[currentPlayer] + roll;
                if (places[currentPlayer] > 11) places[currentPlayer] = places[currentPlayer] - 12;

                System.out.println(players.get(currentPlayer).getName()
                        + "'s new location is "
                        + places[currentPlayer]);
                System.out.println("The category is " + currentCategory());
                askQuestion();
            } else {
                System.out.println(players.get(currentPlayer).getName() + " is not getting out of the penalty box");
                isGettingOutOfPenaltyBox = false;
            }

        } else {

            places[currentPlayer] = places[currentPlayer] + roll;
            if (places[currentPlayer] > 11) places[currentPlayer] = places[currentPlayer] - 12;

            System.out.println(players.get(currentPlayer).getName()
                    + "'s new location is "
                    + places[currentPlayer]);
            System.out.println("The category is " + currentCategory());
            askQuestion();
        }

    }

    public boolean leaveGame(Player currentPlayer) {
        System.out.println("Do you want to leave the game ? Y/N");
        String answer = scanner.nextLine();

        if (answer.toLowerCase().equals("y")) {
            players.remove(currentPlayer);
            return true;
        } else {
            System.out.println("Player " + currentPlayer.getName() + " stay in the game");
            return false;
        }
    }

    private void askQuestion() {

        String s = "";
        if (currentCategory().equals(CategorieEnum.Pop.toString()))
            s = popQuestions.removeFirst();
        popQuestions.addLast(s);
        if (currentCategory().equals(CategorieEnum.Science.toString()))
            s = popQuestions.removeFirst();
        popQuestions.addLast(s);
        if (currentCategory().equals(CategorieEnum.Sport.toString()))
            s = popQuestions.removeFirst();
        popQuestions.addLast(s);
        if (currentCategory().equals(choosenTheme.toString()))
            s = popQuestions.removeFirst();
        popQuestions.addLast(s);



/*
        if (currentCategory().equals(CategorieEnum.Pop.toString()))
            System.out.println(popQuestions.removeFirst());
        if (currentCategory().equals(CategorieEnum.Science.toString()))
            System.out.println(scienceQuestions.removeFirst());
        if (currentCategory().equals(CategorieEnum.Sport.toString()))
            System.out.println(sportsQuestions.removeFirst());
        if (currentCategory().equals(choosenTheme.toString()))
            System.out.println(choosenQuestions.removeFirst());
*/


    }


    public String currentCategory() {
        if (isPreviousPlayerPrison) {
            String returnTheme = nextPlayerTheme.toString();
            isPreviousPlayerPrison = false;
            nextPlayerTheme = null;
            return returnTheme;
        } else {
            if ((places[currentPlayer] == 0) || (places[currentPlayer] == 4) || (places[currentPlayer] == 8)) {
                return CategorieEnum.Pop.toString();
            }
            if (places[currentPlayer] == 1 || (places[currentPlayer] == 5) || (places[currentPlayer] == 9)) {
                return CategorieEnum.Science.toString();
            }
            if (places[currentPlayer] == 2 || (places[currentPlayer] == 6) || (places[currentPlayer] == 10)) {
                return CategorieEnum.Sport.toString();
            }
        }

        return choosenTheme.toString();
    }

    public boolean wasCorrectlyAnswered() {
        players.get(currentPlayer).increaseCombo();
        if (inPenaltyBox[currentPlayer]) {
            if (isGettingOutOfPenaltyBox) {
                basicAwnser.printCorrect();
                /*purses[currentPlayer]++;*/
                players.get(currentPlayer).setScore(players.get(currentPlayer).getScore() + players.get(currentPlayer).getComboGoodAnswer());
                System.out.println(players.get(currentPlayer).getName()
                        + " now has "
                        + players.get(currentPlayer).getScore() /*purses[currentPlayer]*/
                        + " Gold Coins.");
                inPenaltyBox[currentPlayer] = false;
                boolean winner = didPlayerWin();
                currentPlayer++;
                if (currentPlayer == getHowManyPlayers()) currentPlayer = 0;

                return winner;
            } else {
                currentPlayer++;
                if (currentPlayer == getHowManyPlayers()) currentPlayer = 0;
                return true;
            }
        } else {

            basicAwnser.printCorrect();
            players.get(currentPlayer).setScore(players.get(currentPlayer).getScore() + players.get(currentPlayer).getComboGoodAnswer());
            /*purses[currentPlayer]++;*/
            System.out.println(players.get(currentPlayer).getName()
                    + " now has "
                    + players.get(currentPlayer).getScore() /*purses[currentPlayer]*/
                    + " Gold Coins.");

            boolean winner = didPlayerWin();
            currentPlayer++;
            if (currentPlayer == getHowManyPlayers()) currentPlayer = 0;

            return winner;
        }
    }

    public boolean wrongAnswer() {
        basicAwnser.printIncorrect();
        int prisonTimePlayer = players.get(currentPlayer).addIncPlayerPrisonTime();
        System.out.println(players.get(currentPlayer).getName() + " was sent to the penalty box for the " + prisonTimePlayer + " time");

        inPenaltyBox[currentPlayer] = true;
        players.get(currentPlayer).setComboGoodAnswer(0);
        isPreviousPlayerPrison = true;
        currentPlayer++;
        chooseNextTheme();
        if (currentPlayer == getHowManyPlayers()) currentPlayer = 0;
        return true;
    }

    private void chooseNextTheme() {
        System.out.println("Choose the question theme for the next player");
        basicAwnser.printAskTheme(choosenTheme);
        String nextTheme = scanner.nextLine();
        switch (nextTheme) {
            case "1":
                nextPlayerTheme = CategorieEnum.Sport;
                break;
            case "2":
                nextPlayerTheme = CategorieEnum.Science;
                break;
            case "3":
                nextPlayerTheme = CategorieEnum.Pop;
                break;
            case "4":
                nextPlayerTheme = choosenTheme;
                break;
            default:
                nextPlayerTheme = null;
        }
    }


    public void useJoker() {
        if (players.get(currentPlayer).isJokerUsed()) {
            System.out.println("You already used your joker");
        } else {
            players.get(currentPlayer).setJokerUsed(true);
            System.out.println("Joker has been used, no gold have been won");
            currentPlayer++;
            if (currentPlayer == getHowManyPlayers()) currentPlayer = 0;
        }
    }

    public Player getCurentPlayer() {
        return players.get(currentPlayer);
    }

    public List<Player> getPlayers() {
        return players;
    }

    private boolean didPlayerWin() {
        return !(players.get(currentPlayer).getScore() == winValue);
    }
}
