package com.adaptionsoft.games.model;

import java.util.Objects;

public class Player {

    private String name;
    private boolean isLeft;
    private boolean jokerUsed;
    private int comboGoodAnswer;
    private int score;
    int incPrison;

    public Player(String name) {
        this.name = name;
        this.isLeft = false;
        this.jokerUsed = false;
        this.comboGoodAnswer = 0;
        this.score = 0;
        this.incPrison = 0;
    }

    public Player() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return name.equals(player.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                ", isLeft=" + isLeft +
                ", jokerUsed=" + jokerUsed +
                '}';
    }
    public void increaseCombo(){
        this.comboGoodAnswer++;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isLeft() {
        return isLeft;
    }

    public void setLeft(boolean left) {
        isLeft = left;
    }

    public boolean isJokerUsed() {
        return jokerUsed;
    }

    public void setJokerUsed(boolean jokerUsed) {
        this.jokerUsed = jokerUsed;
    }

    public int getComboGoodAnswer() {
        return comboGoodAnswer;
    }

    public void setComboGoodAnswer(int comboGoodAnswer) {
        this.comboGoodAnswer = comboGoodAnswer;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getIncPrison() {
        return incPrison;
    }

    public void setIncPrison(int incPrison) {
        this.incPrison = incPrison;
    }

    public int addIncPlayerPrisonTime (){
        setIncPrison(incPrison +1);
        return incPrison;
    }
}
