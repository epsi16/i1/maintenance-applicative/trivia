package com.adaptionsoft.games.trivia;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import com.adaptionsoft.games.uglytrivia.Game;
import com.adaptionsoft.games.CategorieEnum;
import com.adaptionsoft.games.model.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;

public class SomeTest {

	private Game aGame;
	private Map<Player, Map<String, Integer>> proba;

	@Before
	public void init() throws Exception {
		aGame = new Game(CategorieEnum.Techno.toString());

		aGame.add(new Player("Cheh"));
		aGame.add(new Player("Michel"));
		aGame.add(new Player("Denis"));
		aGame.add(new Player("Renée"));

		proba = new HashMap<>();
		for (Player p : aGame.getPlayers()) {
			for (CategorieEnum c : CategorieEnum.values()) {
				Integer i = 0;
				Map<String, Integer> tempMap;
				if (proba.get(p) == null) {
					tempMap = new HashMap<String, Integer>();
				} else {
					tempMap = proba.get(p);
				}
				tempMap.put(c.toString(), i);
				proba.put(p, tempMap);
			}
		}
	}

	@Test
	public void true_is_true() throws Exception {
		int totalPop = 0;
		int totalTechno = 0;
		int totalSport = 0;
		int totalScience = 0;
		AtomicBoolean isDistributionPopUnfair = new AtomicBoolean(false);
		AtomicBoolean isDistributionSportUnfair = new AtomicBoolean(false);
		AtomicBoolean isDistributionTechnoUnfair = new AtomicBoolean(false);
		AtomicBoolean isDistributionScienceUnfair = new AtomicBoolean(false);
		Random rand = new Random();
		for (int i = 0; i < 5000; i++) {
			aGame.roll(rand.nextInt(5) + 1);
			Map<String, Integer> tempMap = proba.get(aGame.getCurentPlayer());
			Integer tempInteger = proba.get(aGame.getCurentPlayer()).get(aGame.currentCategory());
			tempMap.put(aGame.currentCategory(), tempInteger + 1);
			proba.put(aGame.getCurentPlayer(), tempMap);
			if (aGame.currentCategory() == "Pop") {
				totalPop += 1;
			} else if (aGame.currentCategory() == "Techno") {
				totalTechno += 1;
			} else if (aGame.currentCategory() == "Science") {
				totalSport += 1;
			} else if (aGame.currentCategory() == "Sport") {
				totalScience += 1;
			}
			aGame.wasCorrectlyAnswered();
		}

		int finalTotalSport = totalSport;
		int finalTotalPop = totalPop;
		int finalTotalTechno = totalTechno;
		int finalTotalScience = totalScience;
		proba.entrySet().stream().forEach(e -> {
			Map<String, Integer> tempMap = e.getValue();
			tempMap.entrySet().stream().forEach(f -> {
				if (f.getKey() == "Pop") {
					if (((f.getValue() * 100) / finalTotalPop) <= 20 || ((f.getValue() * 100) / finalTotalPop) >= 30) {
						isDistributionPopUnfair.set(true);
					}
				} else if (f.getKey() == "Techno") {
					if (((f.getValue() * 100) / finalTotalTechno) <= 20 || ((f.getValue() * 100) / finalTotalTechno) >= 30) {
						isDistributionTechnoUnfair.set(true);
					}
				} else if (f.getKey() == "Science") {
					if (((f.getValue() * 100) / finalTotalScience) <= 20 || ((f.getValue() * 100) / finalTotalScience) >= 30) {
						isDistributionScienceUnfair.set(true);
					}
				} else if (f.getKey() == "Sport") {
					if (((f.getValue() * 100) / finalTotalSport) <= 20 || ((f.getValue() * 100) / finalTotalSport) >= 30) {
						isDistributionSportUnfair.set(true);
					}
				}
			});
		});
		boolean res = false;
		if(isDistributionPopUnfair.get() == false && isDistributionTechnoUnfair.get() == false && isDistributionScienceUnfair.get() == false && isDistributionSportUnfair.get() == false) {
			res = true;
		}
		System.out.println("\n \n Resultat des tests \n -----------------------------");

		System.out.println(proba);
		assertTrue(res);
	}
}
